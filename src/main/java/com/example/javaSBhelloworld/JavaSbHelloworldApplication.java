package com.example.javaSBhelloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSbHelloworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSbHelloworldApplication.class, args);
		// System.out.println("Hello World");
	}

@RestController
class HelloWorldController {

    @GetMapping("/hello")
    public String helloWorld() {
        return "Hello, World!";
    }
}

}
